# Zensub

Zensub is a subtheme based on Zen. It creates a baseline theme to use across multiple sites that can then be modified with a custom subtheme based on zensub.
